package org.ieasam.miguelamer.test;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;

import org.iesam.miguelamer.Product;
import org.iesam.miguelamer.ProductNotFoundException;
import org.iesam.miguelamer.ShoppingCartMiguelAmer;
import org.junit.After;
import org.junit.Before;
import org.junit.jupiter.api.Test;

import junit.framework.TestCase;

public class ShoppingCartTest  {

    private ShoppingCartMiguelAmer _bookCart;
    private Product _defaultBook;

    /**
     * Sets up the test fixture.
     * Called before every test case method.
     */
    
    @Before
    protected void prepararTest() {

        _bookCart = new ShoppingCartMiguelAmer();

        _defaultBook = new Product("Extreme Programming", 23.95);
        _bookCart.addItem(_defaultBook);
    }
    
    
    @Test
    protected void compruebaVacio() {
    	_bookCart = new ShoppingCartMiguelAmer();

        _defaultBook = new Product("Extreme Programming", 23.95);
        _bookCart.addItem(_defaultBook);
        
    	_bookCart.empty();
    	assertTrue("el carro está vacío", _bookCart.isEmpty());
    }

    
    @Test
    protected void añadirProducto() {
    	_bookCart = new ShoppingCartMiguelAmer();

        _defaultBook = new Product("Extreme Programming", 23.95);
        _bookCart.addItem(_defaultBook);
        
    	
    	Product product = new Product("Llet", 1);
    	_bookCart.addItem(product);
    	assertEquals(2, _bookCart.getItemCount());
    	assertEquals(product.getPrice()+_defaultBook.getPrice(), _bookCart.getBalance());
    }
    @Test
    protected void productoNoExistente() {
    	_bookCart = new ShoppingCartMiguelAmer();

        _defaultBook = new Product("Extreme Programming", 23.95);
        _bookCart.addItem(_defaultBook);
        Product p2 = new Product("Pala", 25);
        try {
			_bookCart.removeItem(p2);
		} catch (ProductNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println("El producte que intentes treure no estava al carro");
		}

		System.out.println("Feature de Miguel Amer");
        
    }
    /**
     * Tears down the test fixture.
     * Called after every test case method.
     */
    
    @After
    protected void limpiarTest() {
        _bookCart = null;
    }
   
}
