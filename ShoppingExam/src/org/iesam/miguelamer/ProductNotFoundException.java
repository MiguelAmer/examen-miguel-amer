package org.iesam.miguelamer;

/**
 * Exception thrown when a product is not found in a shopping cart.

 */
 
public class ProductNotFoundException extends Exception {
	
	/**
	 * Constructs a ProductNotFoundException.
	 */
	public ProductNotFoundException() {
		super();
	}
}